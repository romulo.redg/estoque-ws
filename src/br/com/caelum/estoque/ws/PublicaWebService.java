package br.com.caelum.estoque.ws;

import javax.xml.ws.Endpoint;

public class PublicaWebService {

	public static void main(String[] args) {
		
		EstoqueWS service = new EstoqueWS();
		String url = "http://0.0.0.0:8090/estoquews";
		
		System.out.println("Sistema rodando " + url +"?wsdl");
		
		Endpoint.publish(url, service);
	}

}
