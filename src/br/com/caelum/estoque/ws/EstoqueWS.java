package br.com.caelum.estoque.ws;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import br.com.caelum.estoque.modelo.item.Filtro;
import br.com.caelum.estoque.modelo.item.Filtros;
import br.com.caelum.estoque.modelo.item.Item;
import br.com.caelum.estoque.modelo.item.ItemDao;
import br.com.caelum.estoque.modelo.item.ItemValidador;
import br.com.caelum.estoque.modelo.item.ListaItens;
import br.com.caelum.estoque.modelo.usuario.AutenticacaoException;
import br.com.caelum.estoque.modelo.usuario.TokenDao;
import br.com.caelum.estoque.modelo.usuario.TokenUsuario;

@WebService
public class EstoqueWS {
	
	private ItemDao dao = new ItemDao();
	
	@WebMethod(operationName="todosOsItens")
    @WebResult(name="itens")
//	@ResponseWrapper(localName = "itens")
	public ListaItens getItens(@WebParam(name="filtros") Filtros filtros){
		System.out.println("chamada metodo getItens");
		List<Item> itensResultado;
		if(filtros==null) {
			itensResultado= dao.todosItens();
			return new ListaItens(itensResultado);
		}else if (filtros.getLista().isEmpty()) {
			itensResultado= dao.todosItens();
			return new ListaItens(itensResultado);
		}else if (filtros.getLista().get(0).getTipo()==null) {
			itensResultado= dao.todosItens();
			return new ListaItens(itensResultado);
		}
			
		List<Filtro> lista = filtros.getLista();
        itensResultado = dao.todosItens(lista);
		return new ListaItens(itensResultado);
	}
	
	@WebMethod(operationName="cadastrarItem")
    @WebResult(name="item")
	public Item cadastrarItem(@WebParam(name="token", header=true) TokenUsuario token,@WebParam(name="item")Item item) throws AutenticacaoException {
		System.out.println("chamando metodo cadastrarItem");
		
		boolean valido = new TokenDao().ehValido(token);
		
		if (!valido) {
			throw new AutenticacaoException("Autorização Falhou");
		}
		
		new ItemValidador(item).validate();
		
		this.dao.cadastrar(item);
		return item;
		
	}

}
